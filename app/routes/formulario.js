var express = require("express");
router = express.Router();
var formCntr = require(__dirname+"/../controllers/formulario");

router.route('/').get(async(req,res,next)=>{
  var result = await formCntr.load();
  res.render("formulario", result);
});

router.route('/').post((req,res,next)=>{
  var newForm = {
    nombre: req.body.nombre,
    dni: req.body.dni,
    incidencia: req.body.incidencia,
    urgencia: req.body.urgencia,
    descripcion: req.body.descripcion,
  }
  formCntr.save(newForm);
  res.redirect("/incidencias")
})

module.exports = router;