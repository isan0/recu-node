require("dotenv").config();
var express = require("express");
var app = express();
var server = require("http").createServer(app);
var mongoose = require("mongoose");
var io = require("socket.io")(server)

// Crear conexion
server.listen(process.env.SERVER_PORT, (err, res) => {
  if (err) console.log("Error en el servidor" + err);
  console.log("Servidor conectado");
})

mongoose.connect(
  `mongodb://${process.env.MONGO_ROOT_USER}:${process.env.MONGO_ROOT_PASSWORD}@${process.env.MONGO_URI}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}?authSource=admin`,
  { useCreateIndex: true, useUnifiedTopology: true, useNewUrlParser: true },
  (err, res) => {
      if (err) console.log(`ERROR: connecting to Database: ${err}`);
      else console.log(`Database Online: ${process.env.MONGO_DB}`);
  }
);

var bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));


app.use(express.static(__dirname + '/public'))

// Vista
app.set("views", __dirname + '/views');
app.set("view engine", "pug")

var formRoutes = require(__dirname+'/app/routes/formulario')
var incRoutes = require(__dirname+'/app/routes/incidencias')
var chatRoutes = require(__dirname+'/app/routes/chat')

// Rutas
app.route('/').get((req, res, next) => {
  res.render("index")
})

app.use("/formulario", formRoutes);
app.use("/incidencias",incRoutes);
app.use("/chat", chatRoutes);

// SOocket.io
io.on("connection",(socket)=>{
  console.log("Socket conectado");
  //socket.user="Pedro";
  //Permite conexion con todos
  socket.on("newMSG",(data)=>{
      socket.broadcast.emit("newMSG",data)
  })
})