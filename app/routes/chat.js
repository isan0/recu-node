var express = require("express");
router = express.Router();
var chatCntr = require(__dirname + "/../controllers/chat");

router.route("/").get(async (req, res, next) => {
  var result = await chatCntr.load();
  res.render("chat", result);
});

module.exports = router;