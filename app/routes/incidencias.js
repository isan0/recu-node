var express = require("express");
router = express.Router();
var incCntr = require(__dirname+"/../controllers/formulario");

router.route('/').get(async(req,res,next)=>{
  var incRes = await incCntr.load();
  res.render("incidencias",{allInc: incRes});
});

router.route('/delete/:id').get(async(req,res,next)=>{
  incCntr.delete(req.params.id);
  res.redirect("/incidencias");
});


router.route('/:num').get(async(req,res,next)=>{
  var incUp = await incCntr.load();
  res.render("incidencias-update",{allInc: incUp, upInc: req.params.num});
});

router.route('/update/:id').post(async(req,res,next)=>{
  incCntr.update(req.params.id, req.body.nombre, req.body.dni, req.body.incidencia, req.body.urgencia, req.body.descripcion);
  res.redirect("/incidencias");
});

module.exports = router;