var mongoose = require("mongoose"),
  Formulario = require("../models/Formulario");

exports.load = async() => {
  var res = await Formulario.find({})
  return res;
};

exports.save = (req) => {
  var newForm = new Formulario(req);
  newForm.save((err,res)=>{
    if(err) console.log(err);
    console.log("Insertado en la BD");
    return res;
  });
};

exports.delete = function(id){
  var delInc = {
    _id:id
  };
  Formulario.deleteOne(delInc,function(err){
    if(err) return handleError(err);
  });
};

exports.update = function(id,nnombre,ndni,nincidencia,nurgencia,ndescripcion){
  var upInc = {
    _id: id
  };
  var newData = {
    $set: {
      nombre: nnombre,
      dni: ndni,
      incidencia: nincidencia,
      urgencia: nurgencia,
      descripcion: ndescripcion
    }
  }
  Formulario.updateOne(upInc, newData, function (err) {
    if (err) return handleError(err);
});
};